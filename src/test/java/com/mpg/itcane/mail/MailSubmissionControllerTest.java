package com.mpg.itcane.mail;

import static com.mpg.itcane.mail.WiserAssertions.assertReceivedMessage;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartRequest;
import org.subethamail.wiser.Wiser;

import com.mpg.itcane.mail.controller.MailSubmissionController;
import com.mpg.itcane.mail.entity.Parameter;
import com.mpg.itcane.mail.entity.ParameterDetail;
import com.mpg.itcane.mail.repository.ParameterDetailRepository;
import com.mpg.itcane.mail.repository.ParameterRepository;
import com.mpg.itcane.mail.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class MailSubmissionControllerTest {

    private Wiser wiser;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    
    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    ParameterDetailRepository parameterDetailRepository;

    @Before
    public void setUp() throws Exception {
        wiser = new Wiser();
        wiser.setPort(1025);
        wiser.start();
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        
        /* Prepare Data */
        Parameter emailConfig = new Parameter();
        emailConfig.setCode(ApplicationConstant.PARAMETER_EMAIL_CONFIG_PROVIDER);
        emailConfig.setFlagActive(true);
        emailConfig.setCreateDate(CommonUtil.getCurrentDateTimestamp());
        emailConfig.setDescription("Email Service");
        parameterRepository.saveAndFlush(emailConfig);

        ParameterDetail provider1 = new ParameterDetail();
        provider1.setCode("ITCANE1");
        provider1.setCreateDate(CommonUtil.getCurrentDateTimestamp());
        provider1.setVariable1("smtp.protossgroup.com");
        provider1.setVariable2("587");
        provider1.setVariable3("contact@protossgroup.com");
        provider1.setVariable4("Pt_11111111");
        provider1.setVariable5("hr@protossgroup.com");
        provider1.setVariable6("Default Mail To");
        provider1.setVariable7("Y");
        provider1.setVariable8("contact@protossgroup.com");
        provider1.setVariable9("1");
        provider1.setVariable10("0");
        
        provider1.setFlagActive(true);
        provider1.setParameter(emailConfig);
        parameterDetailRepository.saveAndFlush(provider1);
        
        Parameter emailTemplate = new Parameter();
        emailTemplate.setCode(ApplicationConstant.PARAMETER_EMAIL_TEMPLATE);
        emailTemplate.setFlagActive(true);
        emailTemplate.setCreateDate(CommonUtil.getCurrentDateTimestamp());
        emailTemplate.setDescription("Email Template");
        parameterRepository.saveAndFlush(emailTemplate);
        
        ParameterDetail template1 = new ParameterDetail();
        template1.setCode("NOTI_SUCCESS");
        template1.setName("Notify_SUCCESS");
        template1.setCreateDate(CommonUtil.getCurrentDateTimestamp());
        template1.setVariable1("แจ้งเตือนความสำเร็จ");
        template1.setVariable2("ข้อความจาก $sender เพื่อให้ทราบถึงความสำเร็จ");
        
        template1.setFlagActive(true);
        template1.setParameter(emailTemplate);
        parameterDetailRepository.saveAndFlush(template1);

    }

    @After
    public void tearDown() throws Exception {
        wiser.stop();
    }
   
    
    @Test
    public void sendMail() throws Exception {
    	String from = "me@localhost";
    	String fromAddress = "me@localhost";
    	String ccAddress="test@localhost";
        String addressTo="someone@localhost";
        String subject="Test sendMail";
        String contentText="Content of sendMail";
        
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMail")
        		.param("from", from)
        		.param("fromAddress", fromAddress)
        		.param("ccAddress", ccAddress)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("Test sendMail");
    }
    
    @Test
    public void sendMail_Case_Attach() throws Exception {
    	String from = "me@localhost";
    	String fromAddress = "me@localhost";
    	String ccAddress="test@localhost";
        String addressTo="someone@localhost";
        String subject="Test sendMail";
        String contentText="Content of sendMail";
    	
    	ClassLoader classLoader = getClass().getClassLoader();
    	
    	File f1 = new File(classLoader.getResource("testMailAttachment/test_attach1.png").getFile());
    	FileInputStream fi1 = new FileInputStream(f1);
        MockMultipartFile fs1 = new MockMultipartFile("upload", f1.getName(), "multipart/form-data",fi1);
        
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMail")
        		.file(fs1)
        		.param("from", from)
        		.param("fromAddress", fromAddress)
        		.param("ccAddress", ccAddress)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("Test sendMail");
    }
    
    @Test
    public void sendMail_Error_addressTo_is_empty() throws Exception {
    	String from = "me@localhost";
    	String fromAddress = "me@localhost";
    	String ccAddress="";
        String addressTo="";
        String subject="Test sendMail";
        String contentText="Content of sendMail";
        
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMail")
        		.param("from", from)
        		.param("fromAddress", fromAddress)
        		.param("ccAddress", ccAddress)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"success\": false,\"message\": \"addressTo is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));
        
    }
    
    @Test
    public void sendMail_Error_subject_is_empty() throws Exception {
    	String from = "me@localhost";
    	String fromAddress = "me@localhost";
    	String ccAddress="";
        String addressTo="someone@localhost";
        String subject="";
        String contentText="Content of sendMail";
        
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMail")
        		.param("from", from)
        		.param("fromAddress", fromAddress)
        		.param("ccAddress", ccAddress)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"success\": false,\"message\": \"subject is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));
        
    }
    
    @Test
    public void sendMailByProviderCode() throws Exception {
    	String from = "me@localhost";
    	String fromAddress = "me@localhost";
    	String ccAddress="test@localhost";
    	String providerCode="ITCANE1";
        String addressTo="someone@localhost";
        String subject="Test sendMailByProviderCode";
        String contentText="Content of sendMailByProviderCode";
        
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByProviderCode")
        		.param("from", from)
        		.param("fromAddress", fromAddress)
        		.param("ccAddress", ccAddress)
        		.param("providerCode", providerCode)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("Test sendMailByProviderCode");
    }
    
    @Test
    public void sendMailByProviderCode_Case_Attachment() throws Exception {
    	String from = "me@localhost";
    	String fromAddress = "me@localhost";
    	String ccAddress="test@localhost";
    	String providerCode="ITCANE1";
        String addressTo="someone@localhost";
        String subject="Test sendMailByProviderCode";
        String contentText="Content of sendMailByProviderCode";
        
        ClassLoader classLoader = getClass().getClassLoader();
    	
    	File f1 = new File(classLoader.getResource("testMailAttachment/test_attach1.png").getFile());
    	FileInputStream fi1 = new FileInputStream(f1);
        MockMultipartFile fs1 = new MockMultipartFile("upload", f1.getName(), "multipart/form-data",fi1);
        
        
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByProviderCode")
        		.file(fs1)
        		.param("from", from)
        		.param("fromAddress", fromAddress)
        		.param("ccAddress", ccAddress)
        		.param("providerCode", providerCode)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("Test sendMailByProviderCode");
    }
    
    @Test
    public void sendMailByProviderCode_Case_addressTo_is_empty() throws Exception {
    	String from = "me@localhost";
    	String fromAddress = "me@localhost";
    	String ccAddress="test@localhost";
    	String providerCode="ITCANE1";
        String addressTo="";
        String subject="Test sendMailByProviderCode";
        String contentText="Content of sendMailByProviderCode";
        
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByProviderCode")
        		.param("from", from)
        		.param("fromAddress", fromAddress)
        		.param("ccAddress", ccAddress)
        		.param("providerCode", providerCode)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
        .andExpect(status().isOk())
        .andExpect(content().json("{\"success\": false,\"message\": \"addressTo is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    
    @Test
    public void sendMailByProviderCode_Case_subject_is_empty() throws Exception {
    	String from = "me@localhost";
    	String fromAddress = "me@localhost";
    	String ccAddress="test@localhost";
    	String providerCode="ITCANE1";
        String addressTo="someone@localhost";
        String subject="";
        String contentText="Content of sendMailByProviderCode";
        
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByProviderCode")
        		.param("from", from)
        		.param("fromAddress", fromAddress)
        		.param("ccAddress", ccAddress)
        		.param("providerCode", providerCode)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
        .andExpect(status().isOk())
        .andExpect(content().json("{\"success\": false,\"message\": \"subject is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    
    @Test
    public void sendBasicMail() throws Exception {
        String addressTo="someone@localhost";
        String subject="Test sendBasicMail";
        String contentText="Content of sendBasicMail";
        
        mockMvc.perform(post("/sendBasicMail")
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("Test sendBasicMail");
    }
    
    @Test
    public void sendBasicMail_Case_addressTo_is_empty() throws Exception {
        String addressTo="";
        String subject="Test sendBasicMail";
        String contentText="Content of sendBasicMail";
        
        mockMvc.perform(post("/sendBasicMail")
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
        .andExpect(status().isOk())
        .andExpect(content().json("{\"success\": false,\"message\": \"addressTo is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    
    @Test
    public void sendBasicMail_Case_subject_is_empty() throws Exception {
        String addressTo="someone@localhost";
        String subject="";
        String contentText="Content of sendBasicMail";
        
        mockMvc.perform(post("/sendBasicMail")
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
        .andExpect(status().isOk())
        .andExpect(content().json("{\"success\": false,\"message\": \"subject is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    
    @Test
    public void sendBasicMailByProviderCode() throws Exception {
    	String providerCode="ITCANE1";
        String addressTo="someone@localhost";
        String subject="Test sendBasicMailByProviderCode";
        String contentText="Content of sendBasicMailByProviderCode";
        
        mockMvc.perform(post("/sendBasicMailByProviderCode")
        		.param("providerCode", providerCode)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("Test sendBasicMailByProviderCode");
    }
    
    @Test
    public void sendBasicMailByProviderCode_Case_addressTo_is_empty() throws Exception {
    	String providerCode="ITCANE1";
        String addressTo="";
        String subject="Test sendBasicMailByProviderCode";
        String contentText="Content of sendBasicMailByProviderCode";
        
        mockMvc.perform(post("/sendBasicMailByProviderCode")
        		.param("providerCode", providerCode)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
        .andExpect(status().isOk())
        .andExpect(content().json("{\"success\": false,\"message\": \"addressTo is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    
    @Test
    public void sendBasicMailByProviderCode_Case_subject_is_empty() throws Exception {
    	String providerCode="ITCANE1";
        String addressTo="someone@localhost";
        String subject="";
        String contentText="Content of sendBasicMailByProviderCode";
        
        mockMvc.perform(post("/sendBasicMailByProviderCode")
        		.param("providerCode", providerCode)
        		.param("addressTo", addressTo)
        		.param("subject", subject)
        		.param("contentText", contentText)
        		)
        .andExpect(status().isOk())
        .andExpect(content().json("{\"success\": false,\"message\": \"subject is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    
    
    @Test
    public void sendMailByTemplate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByTemplate/NOTI_SUCCESS")
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("addressTo", "someone@localhost")
        		.param("sender", "Suriya Eiamerb")
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("แจ้งเตือนความสำเร็จ");
    }

    @Test
    public void sendMailByTemplate_Case_Attachment() throws Exception {
    	ClassLoader classLoader = getClass().getClassLoader();
    	
    	File f1 = new File(classLoader.getResource("testMailAttachment/test_attach1.png").getFile());
    	FileInputStream fi1 = new FileInputStream(f1);
        MockMultipartFile fs1 = new MockMultipartFile("upload", f1.getName(), "multipart/form-data",fi1);
        
    	
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByTemplate/NOTI_SUCCESS")
        		.file(fs1)
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("ccAddress", "test@localhost")
        		.param("addressTo", "someone@localhost")
        		.param("sender", "Suriya Eiamerb")
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("แจ้งเตือนความสำเร็จ");
    }
    
    @Test
    public void sendMailByTemplate_Case_Identify_Provider_Code() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByTemplate/NOTI_SUCCESS")
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("addressTo", "someone@localhost")
        		.param("sender", "Suriya Eiamerb")
        		.param("providerCode", "ITCANE1")
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("แจ้งเตือนความสำเร็จ");
    }
    
    @Test
    public void sendMailByTemplate_Case_Identify_Subject() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByTemplate/NOTI_SUCCESS")
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("addressTo", "someone@localhost")
        		.param("sender", "Suriya Eiamerb")
        		.param("subject", "แจ้งเตือนความสำเร็จ")
        		.param("contentText", "Test System")
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("แจ้งเตือนความสำเร็จ");
    }
    
    @Test
    public void sendMailByTemplate_Case_addressTo_is_empty() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByTemplate/NOTI_SUCCESS")
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("addressTo", "")
        		.param("sender", "Suriya Eiamerb")
        		)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"success\": false,\"message\": \"addressTo is require field\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    

    @Test
    public void sendMailByTemplate_Case_Template_Inactive() throws Exception {
    	ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_TEMPLATE),"NOTI_SUCCESS");
    	parameterDetail.setFlagActive(false);
    	parameterDetailRepository.save(parameterDetail);
    	
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByTemplate/NOTI_SUCCESS")
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("addressTo", "")
        		.param("sender", "Suriya Eiamerb")
        		)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"success\": false,\"message\": \"Template NOTI_SUCCESS Not Found @Parameter EMI002\",\"data\": {},\"error_code\":\"EMIERR003\" }"));

    }
    
    @Test
    public void sendMailByTemplate_Case_ContentText_I_Empty() throws Exception {
    	ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_TEMPLATE),"NOTI_SUCCESS");
    	parameterDetail.setVariable2(null);
    	parameterDetailRepository.save(parameterDetail);
    	
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByTemplate/NOTI_SUCCESS")
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("addressTo", "someone@localhost")
        		.param("sender", "Suriya Eiamerb")
        		)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"success\": false,\"message\": \"No 'contentText' in template NOTI_SUCCESS\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    
    @Test
    public void sendMailByTemplate_Case_subject_template_is_empty() throws Exception {
    	ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_TEMPLATE),"NOTI_SUCCESS");
    	parameterDetail.setVariable1(null);
    	parameterDetailRepository.save(parameterDetail);
    	
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/sendMailByTemplate/NOTI_SUCCESS")
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("addressTo", "someone@localhost")
        		.param("sender", "Suriya Eiamerb")
        		)
                .andExpect(status().isOk())
                .andExpect(content().json("{\"success\": false,\"message\": \"No 'subject' in template NOTI_SUCCESS\",\"data\": {},\"error_code\":\"EMIERR002\" }"));

    }
    
    @Test
    public void sendBasicMailByTemplate() throws Exception {
        mockMvc.perform(post("/sendBasicMailByTemplate/NOTI_SUCCESS")
        		.param("from", "me@localhost")
        		.param("fromAddress", "me@localhost")
        		.param("addressTo", "someone@localhost")
        		.param("sender", "Suriya Eiamerb")
        		)
                .andExpect(status().isOk());
        
        assertReceivedMessage(wiser)
        .from("me@localhost")
        .to("someone@localhost")
        .withSubject("แจ้งเตือนความสำเร็จ");
    }

}
