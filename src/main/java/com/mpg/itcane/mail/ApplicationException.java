package com.mpg.itcane.mail;

public class ApplicationException extends RuntimeException{

	public ApplicationException(String message) {
		super(message);
	}

	
}
