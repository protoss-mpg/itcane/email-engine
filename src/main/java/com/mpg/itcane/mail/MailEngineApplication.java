/**
 * 
 */
/**
 * @author suriya_e@protosstechnology.com
 *
 */
package com.mpg.itcane.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.mpg.itcane.mail.model.MailPropertiesModel;

@SpringBootApplication
public class MailEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailEngineApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer(){
		return new WebMvcConfigurer(){
			@Override
		    public void addCorsMappings(CorsRegistry registry) {
		        registry.addMapping("/**").allowedOrigins("*");
		    }
		};
	}
	
	@Bean
	public MailPropertiesModel mailPropertiesModel() {
		return new MailPropertiesModel();
	}
}
