package com.mpg.itcane.mail.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mpg.itcane.mail.entity.Parameter;

public interface ParameterRepository extends JpaSpecificationExecutor<Parameter>, JpaRepository<Parameter, Long> {

}
