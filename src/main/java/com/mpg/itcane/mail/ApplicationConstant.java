package com.mpg.itcane.mail;

import org.springframework.beans.factory.annotation.Value;

public class ApplicationConstant {
	
	private ApplicationConstant() {
		super();
	} 
	
	@Value("${parameter.mail}")
	public static final String PARAMETER_EMAIL_CONFIG_PROVIDER = "EMI001";
	public static final String PARAMETER_EMAIL_TEMPLATE        = "EMI002";
	
	/* ERROR CODE */
	public static final String ERROR_CODE_PROCESS_FAIL        = "EMIERR001";
	public static final String ERROR_CODE_THROW_EXCEPTION     = "EMIERR002";
	public static final String ERROR_CODE_NO_DATA_FOUND       = "EMIERR003";
	public static final String ERROR_CODE_REQUIRE_DATA        = "EMIERR004";
	

	/* Mail Properties */
	public static final String MAIL_PROPERTIES_SMTP_AUTH 	  = "mail.smtp.auth";
	public static final String MAIL_PROPERTIES_SMTP_STARTTLS  = "mail.smtp.starttls.enable";
	public static final String MAIL_PROPERTIES_SMTP_HOST      = "mail.smtp.host";
	public static final String MAIL_PROPERTIES_SMTP_PORT      = "mail.smtp.port";
	
	public static final String MAIL_CONTENT_TYPE 			  = "text/html; charset=utf-8";
	public static final String UTF_8 			  			  = "UTF-8";
	public static final String UTF_8_LOWER 			  		  = "utf8";
	
	public static final String FIX_ADDRESS_TO_IS_REQUIRE      = "addressTo is require field";
	public static final String FIX_FROM_ADDRESS_IS_REQUIRE    = "fromAddress is require field";
	public static final String FIX_FROM_IS_REQUIRE            = "from is require field";
	public static final String FIX_SUBJECT_IS_REQUIRE         = "subject is require field";
	
	
	
	
	
	
	
	
	
	
}
