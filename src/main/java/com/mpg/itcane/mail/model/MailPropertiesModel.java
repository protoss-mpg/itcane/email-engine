package com.mpg.itcane.mail.model;

import org.springframework.beans.factory.annotation.Value;

import lombok.Data;

@Data
public class MailPropertiesModel {
	
	@Value("${mail.properties.enable:false}")
    private boolean isMailPropertiesEnable;
	@Value("${mail.protocol:}")
    private String protocol;
    @Value("${mail.host:}")
    private String host;
    @Value("${mail.port:}")
    private String port;
    @Value("${mail.smtp.auth:true}")
    private Boolean auth;
    @Value("${mail.smtp.starttls.enable:false}")
    private Boolean starttls;
    @Value("${mail.from:}")
    private String from;
    @Value("${mail.username:}")
    private String usermail;
    @Value("${mail.password:}")
    private String passmail;
    
    public Integer getPortIntValue() {
    	return Integer.parseInt(port);
    }

}
