package com.mpg.itcane.mail.model;

import lombok.Data;

@Data
public class MailModel {
	
	
	String from;
    String fromAddress;
    String addressTo;
    String ccAddress;
    String subject;
    String contentText;
    String host;
	Integer port;
	String username;
	String password;
	String defaultBcc;
	String defaultEmailTo;
	String mainProvider;
	Boolean auth;
	Boolean starttls;
	String protocol="smtp";
	String providerCode ;
    
	public MailModel(String from, String fromAddress, String addressTo, String ccAddress, String subject,
			String contentText) {
		super();
		this.from = from;
		this.fromAddress = fromAddress;
		this.addressTo = addressTo;
		this.ccAddress = ccAddress;
		this.subject = subject;
		this.contentText = contentText;
	}
	
	

	public MailModel() {
		super();
	}



	public MailModel(String addressTo, String subject, String contentText) {
		super();
		this.addressTo = addressTo;
		this.subject = subject;
		this.contentText = contentText;
	}
    
    

}
