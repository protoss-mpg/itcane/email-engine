/**
 * 
 */
/** 
 * @author suriya_e@protosstechnology.com
 *
 */
package com.mpg.itcane.mail.controller;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.ParseException;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import com.mpg.itcane.mail.ApplicationConstant;
import com.mpg.itcane.mail.ApplicationException;
import com.mpg.itcane.mail.entity.ParameterDetail;
import com.mpg.itcane.mail.model.MailModel;
import com.mpg.itcane.mail.model.MailPropertiesModel;
import com.mpg.itcane.mail.model.ResponseModel;
import com.mpg.itcane.mail.repository.ParameterDetailRepository;
import com.mpg.itcane.mail.util.BeansUtil;
import com.mpg.itcane.mail.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class MailSubmissionController {
	
	@Autowired
	ParameterDetailRepository parameterDetailRepository;
	
	@Autowired
	MailPropertiesModel mailPropertiesModel;
	
	
	private Session  getSessionMail(Properties props,MailModel mailModel) {
		log.info("============= Create SessionMail ========");
		Session sessionMail ;
        if(mailPropertiesModel.isMailPropertiesEnable()){
        	sessionMail = Session.getInstance(props,
                    new Authenticator() {
        		@Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mailPropertiesModel.getUsermail(), mailPropertiesModel.getPassmail());
                }
            });
        }else{
        	sessionMail = Session.getInstance(props,
                    new Authenticator() {
        		@Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mailModel.getUsername(), mailModel.getPassword());
                }
            });
        }
        return sessionMail;
	}
	
	
	private JavaMailSenderImpl getMailSender(MailModel mailModel) {
		log.info("============= Create Mail Sender ========");
    	JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties mailProperties = new Properties();
        mailProperties.put(ApplicationConstant.MAIL_PROPERTIES_SMTP_AUTH, mailModel.getAuth());
        mailProperties.put(ApplicationConstant.MAIL_PROPERTIES_SMTP_STARTTLS, mailModel.getStarttls());
        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setHost(mailModel.getHost());
        mailSender.setPort(mailModel.getPort());
        mailSender.setProtocol(mailModel.getProtocol());
        mailSender.setUsername(mailModel.getUsername());
        mailSender.setPassword(mailModel.getPassword());
        
        return mailSender;
	}
	
	private Properties getMailProperties(MailModel mailModel) {
		log.info("============= Create Mail Properties ========");
		Properties props = new Properties();
	    props.put(ApplicationConstant.MAIL_PROPERTIES_SMTP_AUTH, mailModel.getAuth());
	    props.put(ApplicationConstant.MAIL_PROPERTIES_SMTP_STARTTLS, mailModel.getStarttls());
	    props.put(ApplicationConstant.MAIL_PROPERTIES_SMTP_HOST, mailModel.getHost());
	    props.put(ApplicationConstant.MAIL_PROPERTIES_SMTP_PORT, mailModel.getPort());
        
        return props;
	}
	

	private void printLogEmailInformation(MailModel mailModel) {
		log.debug("============= Mail Properties Information ========");
		log.debug("============= host={}",mailModel.getHost());
    	log.debug("============= port={}",mailModel.getPort());
    	log.debug("============= auth={}",mailModel.getAuth());
    	log.debug("============= starttls={}",mailModel.getStarttls());
    	log.debug("============= username={}",mailModel.getUsername());
    	log.debug("============= fromAddress={}",mailModel.getFromAddress());
    	log.debug("============= defaultEmailTo={}",mailModel.getDefaultEmailTo());
    	log.debug("============= defaultBcc={}",mailModel.getDefaultBcc());
    	log.debug("============= mainProvider={}",mailModel.getMainProvider());
    	
	}
	
	private String getMessageSuccess(String addressTo) {
		return "Test send email to "+addressTo+" successful.";
	}
	
	private void setMailModel(MailModel mailModel,ParameterDetail parameterDetail) {
		log.debug("============= Set Mail Model ========");
		mailModel.setHost(parameterDetail.getVariable1());
    	mailModel.setPort(Integer.parseInt(parameterDetail.getVariable2()));
    	mailModel.setUsername(parameterDetail.getVariable3());
    	mailModel.setPassword(parameterDetail.getVariable4());
    	mailModel.setDefaultBcc(parameterDetail.getVariable5());
    	mailModel.setDefaultEmailTo(parameterDetail.getVariable6());
    	mailModel.setMainProvider(parameterDetail.getVariable7());
    	mailModel.setAuth(Boolean.valueOf(parameterDetail.getVariable9()));
    	mailModel.setFrom(parameterDetail.getVariable8());
    	mailModel.setFromAddress(parameterDetail.getVariable8());
    	mailModel.setStarttls(Boolean.valueOf(parameterDetail.getVariable10()));
    	
	}
	

	private void setMailModel(MailModel mailModel) {
		/* Check isMailPropertiesEnable=true for use from properties */
    	if(mailPropertiesModel.isMailPropertiesEnable()){
    		mailModel.setHost(mailPropertiesModel.getHost());
        	mailModel.setPort(mailPropertiesModel.getPortIntValue());
        	mailModel.setProtocol(mailPropertiesModel.getProtocol());
        	mailModel.setAuth(mailPropertiesModel.getAuth());
        	mailModel.setStarttls(mailPropertiesModel.getStarttls());
    	    mailModel.setFrom(mailPropertiesModel.getFrom());
    	    mailModel.setFromAddress(mailPropertiesModel.getFrom());
    	}
	}
	
	/**
	 * Method for replace variable in template
	 * @throws ParseException 
	 **/
	private String getContentFromTemplate(String templateContent,Map mapContext) throws ParseException {
		RuntimeServices runtimeServices;   
		StringReader stringReader;
		SimpleNode simpleNode;
		Template template;
		String returnResult = templateContent;
		
		if(templateContent!=null&&templateContent.trim().length() > 0){
			runtimeServices = RuntimeSingleton.getRuntimeServices();            
			stringReader = new StringReader(templateContent);
			simpleNode = runtimeServices.parse(stringReader, "Email Template");
			 
			template = new Template();
			template.setRuntimeServices(runtimeServices);
			template.setData(simpleNode);
			template.initDocument();
			  
	        /*  add that list to a VelocityContext  */
	        VelocityContext context = new VelocityContext(mapContext);
	        
	        /*  get the Template  */
	        /*  now render the template into a Writer  */
	        StringWriter writer = new StringWriter();
	        template.merge( context, writer );
	        returnResult =  writer.toString();
		}
		
		return returnResult;
		
	}
	
	/**
	 * Method for send basic email without cc and attached file
	 **/
	@PostMapping("/sendBasicMail")
    public ResponseEntity<ResponseModel> sendBasicMail(
                                                  @RequestParam("addressTo") String addressTo,
                                                  @RequestParam("subject") String subject,
                                                  @RequestParam("contentText") String contentText     ) {
		
        ParameterDetail parameterDetail = parameterDetailRepository.findFirstByFlagActiveAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_CONFIG_PROVIDER));
        return this.sendMailImplement(parameterDetail, new MailModel(addressTo,subject,contentText),null);
    }
	
	
	
	/**
	 * Method for send email by Provider Code
	 **/
	@PostMapping("/sendBasicMailByProviderCode")
    public ResponseEntity<ResponseModel> sendBasicMailByProviderCode(
    											  @RequestParam("providerCode") String providerCode,
                                                  @RequestParam("addressTo") String addressTo,
                                                  @RequestParam("subject") String subject,
                                                  @RequestParam("contentText") String contentText) {
		ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_CONFIG_PROVIDER),providerCode);
		return this.sendMailImplement(parameterDetail, new MailModel(addressTo,subject,contentText),null);
    }
	
	/**
	 * Method for send email
	 **/
	@PostMapping("/sendMail")
    public ResponseEntity<ResponseModel> sendMail(
                                                  @RequestParam("from") String from,
                                                  @RequestParam("fromAddress") String fromAddress,
                                                  @RequestParam("addressTo") String addressTo,
                                                  @RequestParam("ccAddress") String ccAddress,
                                                  @RequestParam("subject") String subject,
                                                  @RequestParam("contentText") String contentText,
                                                  MultipartRequest multipartRequest) {
		
        ParameterDetail parameterDetail = parameterDetailRepository.findFirstByFlagActiveAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_CONFIG_PROVIDER));
        return this.sendMailImplement(parameterDetail, 
        		new MailModel(from,fromAddress,addressTo,ccAddress,subject,contentText), 
        		multipartRequest);
        
	}
	
	
	/**
	 * Method for send email by Provider Code
	 **/
	@PostMapping("/sendMailByProviderCode")
    public ResponseEntity<ResponseModel> sendMailByProviderCode(
    		                                      @RequestParam("providerCode") String providerCode,
                                                  @RequestParam("from") String from,
                                                  @RequestParam("fromAddress") String fromAddress,
                                                  @RequestParam("addressTo") String addressTo,
                                                  @RequestParam("ccAddress") String ccAddress,
                                                  @RequestParam("subject") String subject,
                                                  @RequestParam("contentText") String contentText,
                                                  MultipartRequest multipartRequest) {
		
		ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_CONFIG_PROVIDER),providerCode);
		return this.sendMailImplement(parameterDetail, 
				new MailModel(from,fromAddress,addressTo,ccAddress,subject,contentText), 
				multipartRequest);
    }
	
	/**
	 * Method for send email by Template Code
	 **/
	@PostMapping("/sendMailByTemplate/{templateCode}")
    public ResponseEntity<ResponseModel> sendMailByTemplate(MultipartRequest multipartRequest,
    														HttpServletRequest request
    														,@PathVariable String templateCode
    ) {
		return this.sendMailByTemplateImplement(multipartRequest, request, templateCode);
	}
	 
	/**
	 * Method for send email by Template Code
	 **/
	@PostMapping("/sendBasicMailByTemplate/{templateCode}")
    public ResponseEntity<ResponseModel> sendBasicMailByTemplate(HttpServletRequest request
    		,@PathVariable String templateCode
    ) {
		return this.sendMailByTemplateImplement(null, request, templateCode);
	}
	
	private void validateMailModel(MailModel mailModel) {
		if(mailModel.getAddressTo().isEmpty()){
    		throw new ApplicationException(ApplicationConstant.FIX_ADDRESS_TO_IS_REQUIRE);
    	}
    	if(mailModel.getSubject().isEmpty()){
    		throw new ApplicationException(ApplicationConstant.FIX_SUBJECT_IS_REQUIRE);
    	}
	}
	
	/* Implement Method */
	private ResponseEntity<ResponseModel> sendMailImplement(
			ParameterDetail parameterDetail,
			MailModel mailModel,
			MultipartRequest multipartRequest) {
        
		ResponseModel response = new ResponseModel();
        HttpHeaders headers = CommonUtil.getHeaderJson();
        
        if( parameterDetail!=null && parameterDetail.getFlagActive()){
        	this.setMailModel(mailModel, parameterDetail);
        	
        	/* Check isMailPropertiesEnable=true for use from properties */
        	this.setMailModel(mailModel);
        	
        	this.printLogEmailInformation(mailModel);
        	
        	/* Create Mail Sender */
        	this.getMailSender(mailModel);

            /* Create Mail Properties */
            Properties props = this.getMailProperties(mailModel);
            
            /* Create Mail Session */
            Session sessionMail = getSessionMail(props,mailModel);
            
            /* Create Mail Process */
            try {

                MimeMessage message      = new MimeMessage(sessionMail);
                MimeMessageHelper helper = null;
                
                /* Validate Mail Model */
                validateMailModel(mailModel);
            	
            	/* Set Header Information */
                helper = new MimeMessageHelper(message, false, ApplicationConstant.UTF_8);
                message.setSubject(mailModel.getSubject());
                message.setFrom(new InternetAddress(mailModel.getFromAddress(), mailModel.getFrom(), ApplicationConstant.UTF_8_LOWER));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailModel.getAddressTo()));
                helper.setSubject(mailModel.getSubject());
                
                /* CC & BCC Information */
                
                if(BeansUtil.isNotEmpty(mailModel.getCcAddress())) for(String cc:mailModel.getCcAddress().split(",")){
                        helper.addCc(new InternetAddress(cc, mailModel.getFrom(), ApplicationConstant.UTF_8_LOWER));
                }
                helper.addBcc(new InternetAddress(mailModel.getDefaultBcc(), mailModel.getFrom(), ApplicationConstant.UTF_8_LOWER));

                /* Create Multipart Type */
                Multipart multipart = new MimeMultipart();
                MimeBodyPart textPart = new MimeBodyPart();
                textPart.setContent(mailModel.getContentText(), ApplicationConstant.MAIL_CONTENT_TYPE);
                multipart.addBodyPart(textPart);


                /* Upload File to Attached */
                if(multipartRequest != null) for (String key : multipartRequest.getFileMap().keySet()) {
                    MimeBodyPart attachementPart = new MimeBodyPart();
                    MultipartFile file = multipartRequest.getFileMap().get(key);
                    DataSource source = new ByteArrayDataSource(file.getInputStream(), new MimetypesFileTypeMap().getContentType(file.getOriginalFilename()));
                    attachementPart.setDataHandler(new DataHandler(source));
                    attachementPart.setFileName(file.getOriginalFilename());
                    multipart.addBodyPart(attachementPart);
                }


                /* Send Action */
                message.setContent(multipart);
                message.setSentDate(new Date());
                
                Transport.send(message);
 
                
                response.setSuccess(true);
                response.setMessage(getMessageSuccess(mailModel.getAddressTo()));
                response.setError_code(null);
            } catch (Exception e) {
            	log.error(e.getMessage());
    			response.setSuccess(false);
            	response.setError_code(ApplicationConstant.ERROR_CODE_THROW_EXCEPTION);
                response.setMessage(e.getMessage());
            }
        	
        }
        
        return ResponseEntity.ok().headers(headers).body(response);
        
	}
	
	
    private ResponseEntity<ResponseModel> sendMailByTemplateImplement(
    		MultipartRequest multipartRequest,
    		HttpServletRequest request
    		,@PathVariable String templateCode) {
    	
		ResponseModel response = new ResponseModel();
		HttpHeaders headers = CommonUtil.getHeaderJson();
		MailModel mailModel = new MailModel();
		/* Retrieve All Parameter from request */
		Map<String,String> mapContext = new HashMap();
		Map<String,String[]> paramerers = request.getParameterMap();
		for(Entry<String, String[]> entryKey:paramerers.entrySet()){
			String key = entryKey.getKey();
			String value = paramerers.get(key)[0];
			
			switch(String.valueOf(key)) {
				case "from":
					mailModel.setFrom(value);
					break;
				case "fromAddress":
					mailModel.setFromAddress(value);
					break;
				case "addressTo":
					mailModel.setAddressTo(value);
					break;
				case "ccAddress":
					mailModel.setCcAddress(value);
					break;
				case "subject":
					mailModel.setSubject(value);
					break;
				case "contentText":
					mailModel.setContentText(value);
					break;
				case "providerCode":
					mailModel.setProviderCode(value);
					break;
				default:
					mapContext.put(String.valueOf(key), value);
					break;
			}
			
		}
		
		try {
			
			/* Get Template from parameter setup */
			ParameterDetail parameterDetail = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_TEMPLATE),templateCode);
			
			if( parameterDetail!=null && parameterDetail.getFlagActive()){
				
				/* Process to replace data in to template */
				this.prepareDataTemplate(mailModel, parameterDetail, templateCode, mapContext);
	        	
	        	log.debug("subject={}",mailModel.getSubject());
	        	log.debug("contentText={}",mailModel.getContentText());
	        	log.debug("providerCode={}",mailModel.getProviderCode());
	        	
	        	/* Process Send Mail */
	        	ParameterDetail parameterMailConfig ;
	        	if(BeansUtil.isNotEmpty(mailModel.getProviderCode())){
	        		parameterMailConfig = parameterDetailRepository.findFirstByCodeAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_CONFIG_PROVIDER),mailModel.getProviderCode());
	        	}else{
	        		parameterMailConfig = parameterDetailRepository.findFirstByFlagActiveAndParameterCodeIn(Arrays.asList(ApplicationConstant.PARAMETER_EMAIL_CONFIG_PROVIDER));
	        	}
	        	
	        	return this.sendMailImplement(parameterMailConfig, mailModel, multipartRequest);
	        }else{
	        	response.setSuccess(false);
	        	response.setError_code(ApplicationConstant.ERROR_CODE_NO_DATA_FOUND);
	            response.setMessage("Template "+templateCode+" Not Found @Parameter "+ApplicationConstant.PARAMETER_EMAIL_TEMPLATE);
	        }
			
			
		} catch (Exception e) {
			response.setSuccess(false);
        	response.setError_code(ApplicationConstant.ERROR_CODE_THROW_EXCEPTION);
            response.setMessage(e.getMessage());
		}
		
		
		return ResponseEntity.ok().headers(headers).body(response);
	}
	
	private void prepareDataTemplate(MailModel mailModel,ParameterDetail parameterDetail,String templateCode,Map<String,String> mapContext) throws ParseException {

		String subjectEmailTemplate = "";
		String contentEmailTemplate = "";
		
		if( BeansUtil.isNotEmpty(mailModel.getSubject())) {
			subjectEmailTemplate = mailModel.getSubject();
		}else if(BeansUtil.isNotEmpty(parameterDetail.getVariable1())){
    		subjectEmailTemplate = parameterDetail.getVariable1();
    	}else{
    		throw new ApplicationException("No 'subject' in template "+templateCode);
    	}
    	mailModel.setSubject(getContentFromTemplate(subjectEmailTemplate,mapContext));
    	
    	if(BeansUtil.isNotEmpty(parameterDetail.getVariable2())){
    		contentEmailTemplate = parameterDetail.getVariable2();
    		mailModel.setContentText(getContentFromTemplate(contentEmailTemplate,mapContext));
    	}else{
    		throw new ApplicationException("No 'contentText' in template "+templateCode);
    	}
	}
	
	
	
	
}
