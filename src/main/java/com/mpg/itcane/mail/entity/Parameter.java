/**
 * 
 */
/**
 * @author suriya_e@protosstechnology.com
 *
 */
package com.mpg.itcane.mail.entity;


import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Parameter {

    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Version
    @JsonIgnore
    private Long version;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected Timestamp createDate;

    protected String createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected Timestamp updateDate;

    protected String updateBy;

    private Boolean flagActive;

    private String code;
    private String name;
    private String description;
    private String nameMessageCode;
    private Integer numberOfColumn;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "parameter")
    private Set<ParameterDetail> details = new HashSet<ParameterDetail>();


 

}